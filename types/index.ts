// export * from './bookshelves'
// export * from './kiwiflix'

export interface User {
  id: number
  name: string
  email: string
  email_verified_at?: Date
  remember_token?: string
  role: string
  is_blocked: boolean
  created_at: Date
  updated_at: Date
}

export interface Parser {
  filename: string
  path: string
}
