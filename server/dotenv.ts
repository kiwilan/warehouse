export class Dotenv {
  protected constructor(
    protected secretKey: string,
    protected appUrl: string,
  ) {}

  public static load() {
    const config = useRuntimeConfig()
    const self = new Dotenv(config.secretKey, config.appUrl)

    return self
  }

  public getSecretKey() {
    return this.secretKey
  }

  public getAppUrl() {
    return this.appUrl
  }
}
