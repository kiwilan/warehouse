import app from '~/app'
import { validateRequest } from '~/auth'
import { Dotenv } from '~/dotenv'
import { formatFilesToUrl, publicDir, scan } from '~/utils'

export default defineEventHandler(async (event) => {
  const isValid = await validateRequest(event)
  if (!isValid) {
    throw createError({
      status: 403,
      statusMessage: 'Forbidden',
    })
  }

  const dotenv = Dotenv.load()
  const queryParams = getQuery(event)

  const render = queryParams.render ?? false
  const files = await scan(publicDir)

  if (render) {
    return await app(files)
  }

  return {
    files: formatFilesToUrl(files),
    render: `${dotenv.getAppUrl()}/reference?key=${queryParams.key}&render=true`,
  }
})
