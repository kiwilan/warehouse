export default defineEventHandler(() => {
  const tz = 'Europe/Paris'
  const now = new Date().toLocaleString('en-US', {
    timeZone: tz,
  })

  return {
    status: 'online',
    date: now,
    mode: process.env.NODE_ENV,
  }
})
