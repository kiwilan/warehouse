import fs from 'node:fs'
import fsP from 'node:fs/promises'
import nodePath from 'node:path'
import { lookup } from 'mime-types'
import { validateRequest } from '~/auth'
import { publicDir, queryToString } from '~/utils'

export default defineEventHandler(async (event) => {
  const file = queryToString(event, 'file')

  const isValid = await validateRequest(event)
  if (!isValid) {
    throw createError({
      status: 403,
      statusMessage: 'Forbidden',
    })
  }

  const filePath = nodePath.join(publicDir, file)
  const stream = fs.createReadStream(filePath)

  const fileStat = await fsP.stat(filePath)
  const basename = nodePath.basename(filePath)
  const fileMime = lookup(filePath) // application/octet-stream
  const fileSize = fileStat.size

  appendHeaders(event, {
    'Content-Description': 'File Transfer',
    'Content-Transfer-Encoding': 'binary',
    'Expires': -1,
    'Cache-Control': 'must-revalidate, post-check=0, pre-check=0',
    'Pragma': 'public',
    'Content-Type': fileMime,
    'Content-Length': fileSize,
    'Content-Range': `0-${fileSize - 1}/${fileSize}`,
    'Content-Disposition': `attachment; filename="${basename}"`,
  })

  return await sendStream(event, stream)
})
