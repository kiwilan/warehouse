import type { H3Event } from 'h3'
import { Dotenv } from './dotenv'
import { queryToString } from './utils'

export async function validateRequest(event: H3Event): Promise<boolean> {
  const isDev = process.env.NODE_ENV === 'development'
  const key = queryToString(event, 'key')

  const keyIsOk = checkKey(key)

  let msg = 'Forbidden'
  if (isDev) {
    msg = `Forbidden, keyIsOk: ${keyIsOk}`
  }

  if (keyIsOk) {
    return true
  }

  throw createError({
    status: 403,
    statusMessage: msg,
  })
}

function checkKey(key: string): boolean {
  const dotenv = Dotenv.load()
  if (key === dotenv.getSecretKey()) {
    return true
  }

  return false
}
