import type { FsFileItem } from '@kiwilan/filesystem'
import type { H3Event } from 'h3'
import type { Browser, CDPSession, Page } from 'puppeteer'
import { createReadStream } from 'node:fs'
import { cwd } from 'node:process'
import { Readable } from 'node:stream'
import { FsFile } from '@kiwilan/filesystem'
import { fileTypeFromFile } from 'file-type'
import { sendStream } from 'h3'
import puppeteer from 'puppeteer'

export class BrowserKit {
  protected constructor(
    protected url: string,
    protected downloadPath?: string,
    protected browser?: Browser,
    protected page?: Page,
    protected client?: CDPSession,
  ) {}

  public static async make(url: string): Promise<BrowserKit> {
    const self = new this(url)

    const path = `${cwd()}/src/public/downloads`
    if (!await FsFile.exists(path))
      await FsFile.makeDirectory(path, true)
    // self.downloadPath = await self.prepareDownload()
    await self.prepareBrowser()

    return self
  }

  public async googleDriveDownload(): Promise<FsFileItem | undefined> {
    this.page?.emit('Page.setDownloadBehavior', {
      behavior: 'allow',
      downloadPath: './',
    })
    await this.page?.goto(this.url)

    // await this.page?.screenshot({ path: 'screen-1.png' })

    const selector = 'body > div.ndfHFb-c4YZDc.ndfHFb-c4YZDc-AHmuwe-Hr88gd-OWB6Me.dif24c.vhoiae.LgGVmb.bvmRsc.ndfHFb-c4YZDc-vyDMJf-aZ2wEe.ndfHFb-c4YZDc-i5oIFb.ndfHFb-c4YZDc-uoC0bf.ndfHFb-c4YZDc-TSZdd > div.ndfHFb-c4YZDc-Wrql6b > div > div.ndfHFb-c4YZDc-Wrql6b-AeOLfc-b0t70b > div.ndfHFb-c4YZDc-Wrql6b-LQLjdd > div.ndfHFb-c4YZDc-Wrql6b-C7uZwb-b0t70b > div:nth-child(3)'
    await this.page?.waitForSelector(selector)

    await this.page?.waitForNetworkIdle()
    // await this.page?.screenshot({ path: 'screen-2.png' })

    await this.page?.click(selector)
    await this.page?.waitForNetworkIdle()

    await this.page?.screenshot({ path: 'screen-3.png' })

    const pages = await this.browser?.pages()

    if (pages?.length === 3) {
      const lastPage = pages?.[pages.length - 1]

      await this.page?.goto(lastPage?.url() ?? '')
      await this.page?.screenshot({ path: 'screen-4.png' })

      await this.page?.waitForSelector('#uc-download-link')
      await this.page?.click('#uc-download-link')

      await this.page?.screenshot({ path: 'screen-5.png' })
    }

    let files: FsFileItem[] = []
    let ready = false

    while (ready === false) {
      files = await FsFile.allFiles(this.downloadPath!)

      if (files.length !== 0) {
        setTimeout(() => {}, 500)
        if (!files.map(file => file.extension).includes('crdownload'))
          ready = true
      }
    }

    setTimeout(() => {}, 500)
    // await this.browser?.close()

    return files[0]
  }

  public async download(event: H3Event, file: FsFileItem): Promise<void> {
    const path = file.path

    const mimeType = await fileTypeFromFile(file.path)
    event.node.res.setHeader('Content-Disposition', `attachment; filename="${file.name}"`)
    event.node.res.setHeader('Content-Type', mimeType?.mime ?? 'application/octet-stream')

    const filestream = createReadStream(path)
    filestream.on('end', async () => {
      const relativePath = file.path.replace(`/${file.filename}`, '')
      await FsFile.deleteDirectory(relativePath)
    })
    filestream.pipe(event.node.res)

    await sendStream(event, this.readableStream(path))
  }

  private async prepareBrowser() {
    this.browser = await puppeteer.launch({
      // headless: 'new',
      headless: false,
      args: ['--lang="en-US"'],
    })

    this.page = await this.browser.newPage()
    await this.page.setExtraHTTPHeaders({
      'Accept-Language': 'en',
    })

    this.client = await this.page.target().createCDPSession()
    await this.client.send('Page.setDownloadBehavior', {
      behavior: 'allow',
      downloadPath: this.downloadPath,
    })
  }

  private readableStream(path: string): Readable {
    const stream = new Readable({
      read() {},
    })

    stream.push(path)

    return stream
  }
}
