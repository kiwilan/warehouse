import type { H3Event } from 'h3'
import fsp from 'node:fs/promises'
import path from 'node:path'
import { Dotenv } from './dotenv'

export function logging(msg: string) {
  // eslint-disable-next-line no-console
  console.info(msg)
}

export function queryToString(event: H3Event, key: string): string | undefined {
  const query = getQuery(event)
  const value = query[key]
  if (!value) {
    return undefined
  }

  if (Array.isArray(value)) {
    return value.join(',')
  }

  return value.toString()
}

export async function scan(directoryName: string, results: string[] = []) {
  const files = await fsp.readdir(directoryName, { withFileTypes: true })
  for (const f of files) {
    const fullPath = path.join(directoryName, f.name)
    if (f.isDirectory()) {
      await scan(fullPath, results)
    }
    else {
      const basename = path.basename(fullPath)
      if (basename.startsWith('.')) {
        continue
      }

      if (basename.includes('@SynoEAStream')) {
        continue
      }

      let fp = fullPath
      // replace empty spaces with %20
      if (fp.includes(' ')) {
        fp = fp.replace(/ /g, '%20')
      }

      results.push(fp)
    }
  }

  results.sort((a, b) => a.localeCompare(b))

  return results
}

export const publicDir = 'transfer'

export function formatFilesToUrl(files: string[]) {
  const dotenv = Dotenv.load()

  const items: string[] = []
  for (const file of files) {
    items.push(formatFileToUrl(file, dotenv))
  }

  return items
}

export function formatFileToUrl(file: string, dotenv: Dotenv) {
  let f = file.replace(`${publicDir}/`, '')
  f = f.replace(/ /g, '%20')

  return `${dotenv.getAppUrl()}/download?key=${dotenv.getSecretKey()}&file=${f}`
}
