import nodep from 'node:fs/promises'
import nodepc from 'node:process'
import { renderToString } from 'vue/server-renderer'
import render from './components'

interface Options {
  props: {
    files: string[]
    css?: string
  }
}

async function renderDom(options: Options): Promise<string> {
  const css = await nodep.readFile(`${nodepc.cwd()}/server/app/main.css`, 'utf-8')
  options.props.css = css

  const html = await renderToString(render.setup({ ...options.props }))

  return `<!DOCTYPE html>${html}`
}

export default async function dom(files: string[]): Promise<string> {
  return await renderDom({
    props: {
      files,
    },
  })
}
