import fs from 'node:fs'
import { lookup } from 'mime-types'
import { h } from 'vue'
import { Dotenv } from '~/dotenv'
import { formatFileToUrl, publicDir } from '~/utils'

interface Props {
  file: string
  key: number
}

export default {
  props: {
    file: {
      type: String,
      required: true,
    },
    key: {
      type: Number,
      required: true,
    },
  },
  setup(props: Props) {
    let name = props.file.replace(`${publicDir}/`, '')
    name = name.replace(/%20/g, ' ')

    const dotenv = Dotenv.load()
    const url = formatFileToUrl(props.file, dotenv)

    const fileStat = fs.statSync(`${publicDir}/${name}`)

    function formatSizeHRF(size: number) {
      const i = Math.floor(Math.log(size) / Math.log(1024))
      return `${(size / 1024 ** i).toFixed(2)} ${['B', 'KB', 'MB', 'GB', 'TB'][i]}`
    }
    const size = formatSizeHRF(fileStat.size)

    let fileMime = lookup(props.file)
    if (fileMime.toString().length > 20) {
      fileMime = `${fileMime.toString().slice(0, 20)}...`
    }

    const extension = name.split('.').pop()

    return h('li', { class: 'relative flex justify-between gap-x-6 px-4 py-5 bg-gray-800 hover:bg-gray-700 sm:px-6' }, [
      h('div', { class: 'flex min-w-0 gap-x-4' }, [
        h('svg', { 'class': 'size-12 flex-none rounded-full', 'viewBox': '0 0 24 24', 'fill': 'currentColor', 'aria-hidden': true, 'data-slot': 'icon' }, [
          h('path', { 'fill-rule': 'evenodd', 'd': 'M4.01 2L4 22h16V8l-6-6zM13 9V3.5L18.5 9z', 'clip-rule': 'evenodd' }),
        ]),
        h('div', { class: 'min-w-0 flex-auto' }, [
          h('p', { class: 'text-sm/6 font-semibold text-gray-100' }, [
            h('a', { href: url }, [
              h('span', { class: 'absolute inset-x-0 -top-px bottom-0' }),
              name,
            ]),
          ]),
          h('p', { class: 'mt-1 flex text-xs/5 text-gray-300' }, [
            h('span', { class: 'relative truncate hover:underline' }, `Extension: ${extension}`),
          ]),
        ]),
      ]),
      h('div', { class: 'flex shrink-0 items-center gap-x-4' }, [
        h('div', { class: 'hidden sm:flex sm:flex-col sm:items-end' }, [
          h('p', { class: 'text-sm/6 text-gray-300' }, fileMime),
          h('p', { class: 'mt-1 text-xs/5 text-gray-300' }, [
            'Size: ',
            h('time', { datetime: '2023-01-23T13:23Z' }, size),
          ]),
        ]),
        h('svg', { 'class': 'size-5 flex-none text-gray-400', 'viewBox': '0 0 20 20', 'fill': 'currentColor', 'aria-hidden': true, 'data-slot': 'icon' }, [
          h('path', { 'fill-rule': 'evenodd', 'd': 'M8.22 5.22a.75.75 0 0 1 1.06 0l4.25 4.25a.75.75 0 0 1 0 1.06l-4.25 4.25a.75.75 0 0 1-1.06-1.06L11.94 10 8.22 6.28a.75.75 0 0 1 0-1.06Z', 'clip-rule': 'evenodd' }),
        ]),
      ]),
    ])
  },
}
