import type { PropType } from 'vue'
import { h } from 'vue'
import body from './body'
import head from './head'

interface Props {
  files: string[]
  css?: string
}

export default {
  props: {
    files: {
      type: Array as PropType<string[]>,
      required: true,
    },
    css: {
      type: String,
      required: false,
    },
  },
  setup(props: Props) {
    return h('html', {
      lang: 'en',
    }, [
      head.setup({
        title: 'Warehouse',
        css: props.css,
      }),
      body.setup({
        files: props.files,
      }),
    ])
  },
}
