import type { PropType } from 'vue'
import { h } from 'vue'
import fileCard from './file'

interface Props {
  files?: string[]
}

export default {
  props: {
    files: {
      type: Array as PropType<string[]>,
      required: true,
    },
  },
  setup(props: Props) {
    return h('body', [
      h('div', { class: 'parallax' }),
      h('div', { class: 'container md:max-w-3xl mx-auto px-6 relative z-10 py-6' }, [
        h('div', { class: '' }, [
          'Welcome to Warehouse',
        ]),
        h('ul', { class: 'divide-y divide-gray-700 overflow-hidden bg-white shadow-sm ring-1 ring-gray-900/5 sm:rounded-xl mt-6' }, [
          props.files?.map((file, id) =>
            fileCard.setup({
              file,
              key: id,
            }),
          ),
        ]),
      ]),
    ])
  },
}
