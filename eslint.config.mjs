import antfu from '@antfu/eslint-config'

export default antfu({
  ignores: [
    '.vscode/*',
  ],
}, {
  rules: {
    'no-console': 'warn',
    'node/prefer-global/process': 'off',
    '@typescript-eslint/no-namespace': 'off',
  },
})
