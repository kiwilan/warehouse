// https://nitro.unjs.io/config
export default defineNitroConfig({
  runtimeConfig: {
    secretKey: 'secret',
    appUrl: 'http://localhost',
  },

  srcDir: 'server',
  compatibilityDate: '2024-11-07',
})
