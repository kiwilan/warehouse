FROM node:20.17.0-alpine

WORKDIR /app

COPY . .

RUN npm i -g pnpm

RUN rm -rf node_modules && \
  rm -rf .output && \
  rm -rf .nitro

RUN pnpm install
RUN npx tailwindcss -i ./server/app/tailwind.css -o ./server/app/main.css -m
RUN pnpm build

EXPOSE 3000

CMD ["pnpm", "start"]
