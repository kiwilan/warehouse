# Warehouse

A simple static file server with [Nitro](https://nitro.build/).

## Docker

Create `.env` file:

```bash
cp .env.example .env
```

- `PORT`: local port only, let to default with production, default is `3000`
- `NITRO_APP_URL`: app url, change it on production, default is `http://localhost:3000`
- `NITRO_SECRET_KEY`: secret key to allow access to the app, default is `secret`, you can generate it with `openssl rand -hex 16`
- `APP_PORT`: docker port, default is `3000`

Create `transfer` folder:

```bash
mkdir transfer
```

### Build

```sh
docker compose down --remove-orphans
docker compose up --build -d
```

OR

```sh
sudo docker-compose down --remove-orphans
sudo docker-compose up --build -d
```

### Logs

```sh
docker container logs -f warehouse
```

### Tailwind CSS

```bash
pnpm css
```

## Usage

Store any file into `public` folder, and access it via `http://localhost:3000/reference?key=secret` to get list of files.

## BSD 2-Clause

[BSD 2-Clause](LICENSE)
